import os,yaml,csv
from getPlayer import *

def readMatchData():
    # dataDir=os.getcwd()+"/matchData"
    dataDir=os.getcwd()+"/matchData"
    matchID=1
    for file in os.listdir(dataDir):
        players=[]
        print("OPENED FILE: ",file)
        if file == ".DS_Store":
            break
        with open(dataDir+"/"+file,'r') as match:
            playersInn1=[]
            playersInn2=[]
            loadedMatch=yaml.safe_load(match)
            try:
                winningTeam=loadedMatch["info"]["outcome"]["winner"]
                teams=loadedMatch["info"]["teams"]
                city=loadedMatch["info"]["city"]
                date=loadedMatch["info"]["dates"][0]
                print(date)
                print("Date Type: ", type(date))
                venue=loadedMatch["info"]["venue"]
                print("MatchID: ",matchID)
                with open('./extractedData/match.csv','a') as match:
                    writer=csv.writer(match,delimiter=',')
                    writer.writerow([matchID,teams[0],teams[1],winningTeam,city,date,venue])
                match.close()
            except Exception as e:
                print("Exception OCCURED")
                winningTeam="Draw"
                teams=loadedMatch["info"]["teams"]
                city=loadedMatch["info"]["city"]
                date=loadedMatch["info"]["dates"]
                venue=loadedMatch["info"]["venue"]
                print("MatchID: ",matchID)
                with open('./extractedData/match.csv','a') as match:
                    writer=csv.writer(match,delimiter=',')
                    writer.writerow([matchID,teams[0],teams[1],winningTeam,city,date,venue])
                match.close()
            firstInnTeam=loadedMatch["innings"][0]["1st innings"]["team"]
            firstInningsDeliveries=loadedMatch["innings"][0]["1st innings"]["deliveries"]
            try:
                secondInnTeam=loadedMatch["innings"][1]["2nd innings"]["team"]
                secondInningsDeliveries=loadedMatch["innings"][1]["2nd innings"]["deliveries"]
            except Exception as e:
                print ("No seccond Inns") 

            ng=0 #New Guy(new player)
            #---------------First Innings--------------------------
            for ball in range(0,len(firstInningsDeliveries)-1):
                # players=[]
                batData={} #batsman
                SbatData={} #second batsman
                bowlData={} #bowler
                key= next(iter(firstInningsDeliveries[ball]))
                # print(type(firstInningsDeliveries[]))
                batName=firstInningsDeliveries[ball][key]["batsman"]
                non_striker=firstInningsDeliveries[ball][key]["non_striker"]
                bowlName=firstInningsDeliveries[ball][key]["bowler"]
                #Store each newly discovered player in a list; then iterate list in reverse to add wkt info
                # list must be declared outside of loop
                if batName not in playersInn1:
                    ng+=1
                    print("NEW GUY"+str(ng))
                    playersInn1.append(batName)
                    try:
                        batData.update(getPlayerData(batName,firstInnTeam))#pid, role, name, stats
                        batData.update({"matchID":matchID,"Team":firstInnTeam,"name":batName})
                        players.append(batData)
                    except Exception as e:
                        print("PRINT EXCEPTION IN GETTING DATA", e)
                        write_to_errorLog(matchID,file,firstInnTeam,batName)
                if non_striker not in playersInn1:
                    ng+=1
                    print("NEW GUY"+str(ng))
                    playersInn1.append(non_striker)
                    try:
                        SbatData.update(getPlayerData(non_striker,firstInnTeam))
                        SbatData.update({"matchID":matchID,"Team":firstInnTeam,"name":non_striker})
                        players.append(SbatData)
                    except Exception as e:
                        print("PRINT EXCEPTION IN GETTING DATA", e)
                        write_to_errorLog(matchID,file,firstInnTeam,non_striker)
                if bowlName not in playersInn2:
                    ng+=1
                    print("NEW GUY"+str(ng))
                    playersInn2.append(bowlName)
                    try:
                        bowlData.update(getPlayerData(bowlName,secondInnTeam))
                        bowlData.update({"matchID":matchID,"Team":secondInnTeam,"name":bowlName})
                        players.append(bowlData)
                    except Exception as e:
                        print("PRINT EXCEPTION IN GETTING DATA", e)
                        write_to_errorLog(matchID,file,secondInnTeam,bowlName)
                if "wicket" in firstInningsDeliveries[ball][key]:
                    wkt=wicket(firstInningsDeliveries[ball][key],bowlName)
                    for i in range(len(players)-1,-1,-1):
                        if players[i]["name"]==wkt[1]:
                            players[i].update(wkt[0])
            #----------------End First Innings----------------------

            #----------------Second Innings-------------------------
            try:
                for ball in range(0,len(secondInningsDeliveries)-1):
                    batData={} #batsman
                    SbatData={} #second batsman
                    bowlData={} #bowler
                    key= next(iter(secondInningsDeliveries[ball]))
                    # print(type(firstInningsDeliveries[]))
                    batName=secondInningsDeliveries[ball][key]["batsman"]
                    non_striker=secondInningsDeliveries[ball][key]["non_striker"]
                    bowlName=secondInningsDeliveries[ball][key]["bowler"]
                    if batName not in playersInn2:
                        ng+=1
                        print("NEW GUY"+str(ng))
                        playersInn2.append(batName)
                        try:
                            batData.update(getPlayerData(batName,secondInnTeam))#pid, role, name, stats
                            batData.update({"matchID":matchID,"Team":secondInnTeam,"name":batName})
                            players.append(batData)
                        except Exception as e:
                            print("PRINT EXCEPTION IN GETTING DATA", e)
                            write_to_errorLog(matchID,file,secondInnTeam,batName)
                    if non_striker not in playersInn2:
                        ng+=1
                        print("NEW GUY"+str(ng))
                        playersInn2.append(non_striker)
                        try:
                            SbatData.update(getPlayerData(non_striker,secondInnTeam))
                            SbatData.update({"matchID":matchID,"Team":secondInnTeam,"name":non_striker})
                            players.append(SbatData)
                        except Exception as e:
                            print("PRINT EXCEPTION IN GETTING DATA", e)
                            write_to_errorLog(matchID,file,secondInnTeam,non_striker)
                    if bowlName not in playersInn1:
                        ng+=1
                        print("NEW GUY"+str(ng))
                        playersInn1.append(bowlName)
                        try:
                            bowlData.update(getPlayerData(bowlName,firstInnTeam))
                            bowlData.update({"matchID":matchID,"Team":firstInnTeam,"name":bowlName})
                            players.append(bowlData)
                        except Exception as e:
                            print("PRINT EXCEPTION IN GETTING DATA", e)
                            write_to_errorLog(matchID,file,firstInnTeam,bowlName)
                    if "wicket" in secondInningsDeliveries[ball][key]:
                        wkt=wicket(secondInningsDeliveries[ball][key],bowlName)
                        for i in range(len(players)-1,-1,-1):
                            if players[i]["name"]==wkt[1]:
                                players[i].update(wkt[0])
                #----------------End Second Innings----------------------
            except Exception as e:
                print("No 2nd Innings")
        # print(players)
        #write players to csv file
        matchDataFile = "./extractedData/matchdata.csv"
        batStatsFile = "./extractedData/batStatsData.csv"
        bowlStatsFile = "./extractedData/bowlStatsData.csv"
        columns=['matchID','playerId','fullName','role','batingStyle','bowlingStyle','outer','wicket','Team']
        bowlColumns=['playerId','10','BBM','Balls','Inns','Mat','BBI','SR','5w','4w','Econ','Wkts','Runs','Ave']
        batColumns=['playerId','BF','Inns','Mat','NO','SR','4s','50','Runs','6s','HS','Ave','100','St','Ct']
        with open (matchDataFile,'a') as datafile:
            writer= csv.DictWriter(datafile,fieldnames=columns,extrasaction='ignore')
            # writer.writeheader()
            for data in players:
                writer.writerow(data)
        datafile.close()
        #Write bowling stats to file
        with open (bowlStatsFile,'a') as bowlFile:
            writer = csv.DictWriter(bowlFile,fieldnames=bowlColumns,extrasaction='ignore')
            for data in players:
                bowlData={"playerId":data['playerId']}
                bowlData.update(data['bowling'])
                # print("BOWLDATA: ",bowlData)
                writer.writerow(bowlData)
        bowlFile.close()
        #Write batting stats to file
        with open(batStatsFile,'a') as batFile:
            writer=csv.DictWriter(batFile,fieldnames=batColumns,extrasaction='ignore')
            for data in players:
                batData={'playerId':data['playerId']}
                batData.update(data['batting'])
                writer.writerow(batData)
            batFile.close()
        print("Inc MatchID")
        matchID+=1
        # return players
    return str(winningTeam+"\n\n"+str(teams))
def wicket(ball,bowlName):
    try:
        outed=ball["wicket"]["player_out"]
        wicket=ball["wicket"]["kind"]
        if wicket not in ["bowled","lbw","caught and bowled"]:
            outer=ball["wicket"]['fielders'][0]
            return [{"wicket":wicket,"outer":outer},outed]
        else:
            print("outed: ",outed," ,wicket: ",wicket," ,outer: ",bowlName)
            return [{"wicket":wicket,"outer":bowlName},outed]
    except Exception as e:
            print("EXCEPTION OCCURED")
            print(e)
            return [{"wicket":"None"},None]
def write_to_errorLog(matchID,file,Team,Name):
    logFile="./extractedData/error_log.csv"
    logColumns=['file','matchID','team','name']
    with open(logFile,'a') as log:
        writer=csv.DictWriter(log,fieldnames=logColumns)
        writer.writerow({'file':file,'matchID':matchID,'team':Team,'name':Name})
        log.close()
readMatchData()
