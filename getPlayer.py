# from googlesearch import search
import requests, sys,time
sys.path.insert(0,'/Users/altonbodley/Documents/CricDSS/TranMatchData/xgoogle/xgoogle')
from search import GoogleSearch, SearchError
from random import randint

def getPlayerData(name,team):
    # try:
    # print(name+" cricinfo")
    string=name+" "+team+" espncricinfo player profile"
    query=GoogleSearch(str(string)) #Google search query
    print("String: ",string)
    query.results_per_page=3
    time.sleep(randint(3,10))#pause exe for n sec (Trying to prevent http 429 error)
    urls=query.get_results()
    if urls == None:
        # print("Going again")
        query=GoogleSearch(str(string.split(" ",1)[1])) #Google search query
        # print("String: ",string)
        query.results_per_page=3
        urls=query.get_results()
    # print("Type: ", type(urls))
    # print(urls)
    for url in urls:
        # print(url)
        playerId=(url.split('/')[-1][:-5])
        print(playerId)
        return getPlayerProf(playerId)
    # except SearchError, e:
    #     print "Search failed: %s" % e

def getPlayerProf(playerId):
    # try:
    baseURL="https://cricapi.com/api/playerStats?apikey=nFIL3k42c1Wa0gIgBwyaRY2oypR2&pid="
    data=requests.get(baseURL+playerId)
    plrProf=(data.json())
    # print(plrProf['battingStyle'])
    bowlStats=plrProf["data"]["bowling"]["ODIs"]
    batStats=plrProf["data"]["batting"]["ODIs"]
    name=plrProf["name"]
    role=plrProf["playingRole"]
    # print(bowlStats)
    return {"playerId":plrProf['pid'],"fullName":name,"role":role,"batingStyle":plrProf['battingStyle'],"bowlingStyle":plrProf['bowlingStyle'],"bowling":bowlStats,"batting":batStats}
    # except Exception as ex:
    #     print(ex)
    #     print("GET Player EXCEPTION OCCURED")
    #     return {"Error":"nodata"}
